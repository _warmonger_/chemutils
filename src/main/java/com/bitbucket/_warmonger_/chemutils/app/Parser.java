package com.bitbucket._warmonger_.chemutils.app;

public class Parser
{
	/*
	 * Repository of subscript and superscript characters First 10 characters
	 * (indexes from 0 to 9 inclusively) represents digits from 0 to 9 respectively
	 * Next characters are mapped by constants
	 */
	private static char[] subscripts, superscripts, ref;

	static
	{
		ref = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '-' };
		subscripts = new char[] { '₀', '₁', '₂', '₃', '₄', '₅', '₆', '₇', '₈', '₉', '₊', '₋' };
		superscripts = new char[] { '⁰', '¹', '²', '³', '⁴', '⁵', '⁶', '⁷', '⁸', '⁹', '⁺', '⁻' };
	}

	static final int PLUS  = 10;
	static final int MINUS = 11;

	public static String ToCode(String s)
	{
		StringBuffer res = new StringBuffer();

		char c;

		boolean numMode = false, chargeMode = false;
		for(int i = 0;i < s.length();i++)
		{
			c = s.charAt(i);
			
			if (!numMode && IsSub(c))
			{
				numMode = true;

				res.append('[');
			}
			else if (numMode && !IsSub(c))
			{
				numMode = false;

				res.append(']');
			}
			
			if (!chargeMode && (c == GetSuper(PLUS) || c == GetSuper(MINUS)))
			{
				chargeMode = true;

				res.append('{');
			}
			else if (chargeMode && !IsSuper(c))
			{
				chargeMode = false;

				res.append('}');
			}

			res.append(ToDefault(c));
		}

		return res.toString();
	}

	public static String ToBeauty(String s)
	{
		StringBuffer res = new StringBuffer();

		char c;

		for(int i = 0;i < s.length();i++)
		{
			c = s.charAt(i);

			if (c == '[')
			{
				i++;

				for(;i < s.length() && s.charAt(i) != ']';i++)
				{
					c = s.charAt(i);

					res.append(ToSub(c));
				}
			}
			else if(c == '{')
			{
				i++;

				for(;i < s.length() && s.charAt(i) != '}';i++)
				{
					c = s.charAt(i);

					res.append(ToSuper(c));
				}
			}
			else
			{
				res.append(c);
			}
		}

		return res.toString();
	}

	public static String Update(String s, char c, int pos)
	{
		StringBuilder res = new StringBuilder();
		res.append(s.substring(0, pos));

		int prev = Prev(s, pos);
		int next = Next(s, pos);

		char prevC = -1 < prev ? s.charAt(prev) : 0;
		char nextC = -1 < next ? s.charAt(next) : 0;
		
		if((c == '+' || c == '-') && (prevC != '+' && (0 < pos && s.charAt(pos - 1) != ' ')))
		{
			res.append(ToSuper(c));
		}
		else if (Character.isUpperCase(prevC) || Character.isLowerCase(nextC) || IsSubDigit(prevC) || IsSubDigit(nextC)
				|| prevC == ')')
		{
			res.append(ToSub(c));
		}
		else if (IsSuper(prevC) || IsSuper(nextC))
		{
			res.append(ToSuper(c));
		}
		else if (prevC == 0 || prevC == '+' || Character.isDigit(prevC) || Character.isDigit(nextC))
		{
			res.append(ToDefault(c));
		}
		else
		{			
			return null;
		}

		res.append(s.substring(pos, s.length()));
		return res.toString();
	}

	public static int Prev(String s, int pos)
	{
		if (0 < pos && pos <= s.length())
		{
			int res = pos - 1;
			for(;0 <= res && Character.isWhitespace(s.charAt(res));res--)
				;

			return res;
		}

		return -1;
	}

	public static int Next(String s, int pos)
	{
		if (0 <= pos && pos < s.length())
		{
			int res = pos;
			for(;res < s.length() && Character.isWhitespace(s.charAt(res));res++)
				;

			return res;
		}

		return -1;
	}

	public static int GetIndex(char c)
	{
		int res = GetRefIndex(c);

		if (res < 0)
		{
			res = GetSubIndex(c);

			if (res < 0)
			{
				res = GetSuperIndex(c);
			}
		}

		return res;
	}

	public static int GetRefIndex(char c)
	{
		for(int i = 0;i < ref.length;i++)
		{
			if (c == ref[i])
			{
				return i;
			}
		}

		return -1;
	}

	public static int GetSubIndex(char c)
	{
		for(int i = 0;i < subscripts.length;i++)
		{
			if (c == subscripts[i])
			{
				return i;
			}
		}

		return -1;
	}

	public static int GetSuperIndex(char c)
	{
		for(int i = 0;i < superscripts.length;i++)
		{
			if (c == superscripts[i])
			{
				return i;
			}
		}

		return -1;
	}

	public static boolean IsSuper(char c)
	{
		return 0 <= GetSuperIndex(c);
	}

	public static boolean IsSub(char c)
	{
		return 0 <= GetSubIndex(c);
	}

	public static boolean IsDigit(char c)
	{
		return Character.isDigit(c) || IsSuperDigit(c) || IsSubDigit(c);
	}

	public static boolean IsSuperDigit(char c)
	{
		int res = GetSuperIndex(c);

		return 0 <= res && res < 10;
	}

	public static boolean IsSubDigit(char c)
	{
		int res = GetSubIndex(c);

		return 0 <= res && res < 10;
	}

	public static char GetDefault(int i)
	{
		return i < 0 ? 0 : ref[i];
	}

	public static char GetSub(int i)
	{
		return i < 0 ? 0 : subscripts[i];
	}

	public static char GetSuper(int i)
	{
		return i < 0 ? 0 : superscripts[i];
	}

	public static char ToDefault(char c)
	{
		int res = GetRefIndex(c);

		if (res < 0)
		{
			res = GetSubIndex(c);

			if (res < 0)
			{
				res = GetSuperIndex(c);
			}
		}

		return res < 0 ? c : ref[res];
	}

	public static char ToSub(char c)
	{
		int res = GetRefIndex(c);

		if (res < 0)
		{
			res = GetSubIndex(c);

			if (res < 0)
			{
				res = GetSuperIndex(c);
			}
		}

		return res < 0 ? c : subscripts[res];
	}

	public static char ToSuper(char c)
	{
		int res = GetRefIndex(c);

		if (res < 0)
		{
			res = GetSubIndex(c);

			if (res < 0)
			{
				res = GetSuperIndex(c);
			}
		}

		return res < 0 ? c : superscripts[res];
	}
}
