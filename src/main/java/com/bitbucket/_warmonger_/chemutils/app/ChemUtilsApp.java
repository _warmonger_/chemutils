package com.bitbucket._warmonger_.chemutils.app;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import com.bitbucket._warmonger_.chemutils.Main;
import com.bitbucket._warmonger_.chemutils.input.EntryParser;
import com.bitbucket._warmonger_.chemutils.input.Equation;
import com.bitbucket._warmonger_.chemutils.log.ILogger;
import com.bitbucket._warmonger_.chemutils.log.LogLevel;
import com.bitbucket._warmonger_.chemutils.log.Logger;

public class ChemUtilsApp implements ILogger
{
	private final ILogger       logger;

	private static ChemUtilsApp instance = null;

	private JFrame              frmChemutils;
	private JTextField          leftTextField;
	private JTextField          rightTextField;
	private JTextField          leftResTextField;
	private JTextField          rightResTextField;

	/**
	 * @wbp.parser.constructor
	 */
	public ChemUtilsApp(ILogger logger)
	{
		instance = this;
		this.logger = logger;

		Initialize();
	}

	public ChemUtilsApp()
	{
		this(new Logger());
	}

	public static ChemUtilsApp Instance()
	{
		return instance;
	}

	@Override
	public boolean Log(LogLevel level, String msg)
	{
		return logger.Log(level, msg);
	}

	@Override
	public boolean Log(LogLevel level, String title, String msg)
	{
		return logger.Log(level, title, msg);
	}

	@Override
	public boolean Log(Throwable t)
	{
		return logger.Log(t);
	}

	public boolean Open()
	{
		final AtomicBoolean openResult = new AtomicBoolean(false);

		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				synchronized (openResult)
				{
					try
					{
						frmChemutils.setVisible(true);

						openResult.set(true);
					}
					catch(Throwable t)
					{
						Log(t);
					}

					openResult.notify();
				}
			}
		});

		synchronized (openResult)
		{
			try
			{
				openResult.wait();
			}
			catch(Throwable t)
			{
				Log(t);
			}
		}

		return openResult.get();
	}

	private void Initialize()
	{
		frmChemutils = new JFrame();
		frmChemutils.addWindowListener(new WindowAdapter()
		{
			@Override
			public void windowClosed(WindowEvent arg0)
			{
				ChemUtilsApp.instance = null;
			}
		});
		
		frmChemutils.setTitle("ChemUtils " + Main.VERSION);
		frmChemutils.setBounds(100, 100, 670, 422);
		frmChemutils.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		frmChemutils.getContentPane().setLayout(gridBagLayout);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBackground(Color.LIGHT_GRAY);

		GridBagConstraints gbc_tabbedPane = new GridBagConstraints();
		gbc_tabbedPane.fill = GridBagConstraints.BOTH;
		gbc_tabbedPane.gridx = 0;
		gbc_tabbedPane.gridy = 1;
		frmChemutils.getContentPane().add(tabbedPane, gbc_tabbedPane);

		JPanel panel = new JPanel();
		tabbedPane.addTab("Stoichiometry", null, panel, null);
		panel.setLayout(null);

		JLabel reactionLabel = new JLabel("REACTION");
		reactionLabel.setHorizontalAlignment(SwingConstants.CENTER);
		reactionLabel.setBounds(10, 11, 629, 14);
		panel.add(reactionLabel);

		leftTextField = new JTextField();
		leftTextField.setText("Cu + HNO");
		leftTextField.addKeyListener(new KeyAdapter()
		{
			@Override
			public void keyTyped(KeyEvent arg0)
			{
				JTextField cmp = leftTextField;
				
				if(Character.isWhitespace(arg0.getKeyChar()) || arg0.getKeyChar() == '\b')
				{
					return;
				}
				
				int pos = cmp.getCaretPosition();
				String res = Parser.Update(cmp.getText(), arg0.getKeyChar(), pos);

				if(res != null)
				{
					arg0.consume();
					cmp.setText(res);
					cmp.setCaretPosition(pos + 1);
				}
			}
		});
		leftTextField.setFont(new Font("Tahoma", Font.PLAIN, 17));
		leftTextField.setBounds(10, 36, 281, 41);
		panel.add(leftTextField);
		leftTextField.setColumns(10);

		JLabel arrow0Label = new JLabel("→");
		arrow0Label.setFont(new Font("Tahoma", Font.BOLD, 26));
		arrow0Label.setHorizontalAlignment(SwingConstants.CENTER);
		arrow0Label.setBounds(10, 36, 629, 41);
		panel.add(arrow0Label);

		rightTextField = new JTextField();
		rightTextField.setText("Cu(NO) + NO + HO");
		rightTextField.addKeyListener(new KeyAdapter()
		{
			@Override
			public void keyTyped(KeyEvent arg0)
			{
				JTextField cmp = rightTextField;
				
				if(Character.isWhitespace(arg0.getKeyChar()) || arg0.getKeyChar() == '\b')
				{
					return;
				}
				
				int pos = cmp.getCaretPosition();
				String res = Parser.Update(cmp.getText(), arg0.getKeyChar(), pos);

				if(res != null)
				{
					arg0.consume();
					cmp.setText(res);
					cmp.setCaretPosition(pos + 1);
				}
			}
		});
		rightTextField.setHorizontalAlignment(SwingConstants.LEFT);
		rightTextField.setFont(new Font("Tahoma", Font.PLAIN, 17));
		rightTextField.setColumns(10);
		rightTextField.setBounds(358, 36, 281, 41);
		panel.add(rightTextField);

		JButton balButton = new JButton("TRY BALANCE");
		balButton.addMouseListener(new MouseAdapter()
		{

			@Override
			public void mouseClicked(MouseEvent arg0)
			{
				String line = Parser.ToCode(leftTextField.getText() + " -> " + rightTextField.getText());

				Equation equation;

				try
				{
					equation = EntryParser.Parse(line);
					equation.Reset();
				}
				catch(Throwable t)
				{
					JOptionPane.showMessageDialog(null, "Invalid reaction structure!", "Reaction Parsing Error",
							JOptionPane.ERROR_MESSAGE);

					Log(t);

					return;
				}

				equation.Solve();

				String[] res = equation.toString().split("\\-\\>");
				leftResTextField.setText(Parser.ToBeauty(res[0]).trim());
				rightResTextField.setText(Parser.ToBeauty(res[1]).trim());
			}
		});
		balButton.setBounds(10, 88, 629, 23);
		panel.add(balButton);

		leftResTextField = new JTextField();
		leftResTextField.setFont(new Font("Tahoma", Font.PLAIN, 17));
		leftResTextField.setColumns(10);
		leftResTextField.setBounds(10, 122, 281, 41);
		panel.add(leftResTextField);

		JLabel arrow1Label = new JLabel("→");
		arrow1Label.setHorizontalAlignment(SwingConstants.CENTER);
		arrow1Label.setFont(new Font("Tahoma", Font.BOLD, 26));
		arrow1Label.setBounds(10, 121, 629, 41);
		panel.add(arrow1Label);

		rightResTextField = new JTextField();
		rightResTextField.setFont(new Font("Tahoma", Font.PLAIN, 17));
		rightResTextField.setColumns(10);
		rightResTextField.setBounds(358, 122, 281, 41);
		panel.add(rightResTextField);
	}
}
