package com.bitbucket._warmonger_.chemutils.input;

import java.util.List;

public interface IEntry
{	
	public void Unfold(List<IEntry> list);
	
	public int GetQuantity();	
	public void SetQuantity(int quantity);
	public void UpdateQuantity(int change);
	
	public int GetCharge();	
	public void SetCharge(int charge);
	public void UpdateCharge(int change);
	
	public boolean DoesMatch(IEntry en1);
}
