package com.bitbucket._warmonger_.chemutils.input;

import java.util.ArrayList;
import java.util.List;

import com.bitbucket._warmonger_.chemutils.math.Calculator;

public class Equation implements IEntry
{
	List<Word> left, right;
	
	public Equation(List<Word> left, List<Word> right)
	{
		this.left = left;
		this.right = right;
	}
	
	public Equation()
	{
		left = new ArrayList<Word>();
		right = new ArrayList<Word>();
	}
	
	public List<Word> GetLeft()
	{
		return left;
	}

	public List<Word> GetRight()
	{
		return right;
	}

	@Override
	public String toString()
	{
		StringBuilder res = new StringBuilder();
		
		if(!left.isEmpty())
		{
			res.append(left.get(0));
			
			for(int i = 1; i < left.size(); i++)
			{
				res.append(" + ");
				res.append(left.get(i));
			}
		}
		
		res.append(" -> ");
		
		if(!right.isEmpty())
		{
			res.append(right.get(0));
			
			for(int i = 1; i < right.size(); i++)
			{
				res.append(" + ");
				res.append(right.get(i));
			}
		}
		
		return res.toString();
	}
	
	@Override
	public void Unfold(List<IEntry> list)
	{
		for(Word w : left)
		{
			w.Unfold(list);
		}
		
		for(Word w : right)
		{
			w.Unfold(list);
		}
	}

	@Override
	public int GetQuantity()
	{
		return 1;
	}

	@Override
	public void SetQuantity(int quantity)
	{
		return;
	}

	@Override
	public void UpdateQuantity(int change)
	{
		return;
	}

	@Override
	public int GetCharge()
	{
		return 0;
	}

	@Override
	public void SetCharge(int charge)
	{
		return;
	}

	@Override
	public void UpdateCharge(int change)
	{
		return;
	}

	@Override
	public boolean DoesMatch(IEntry en1)
	{
		return en1 instanceof Equation;
	}

	public boolean Solve()
	{		
		int[] res = Calculator.Calc(left, right);
		
		int i = 0;
		for(Word w : left)
		{
			w.SetQuantity(res[i++]);
		}
		
		for(Word w : right)
		{
			w.SetQuantity(res[i++]);
		}
		
		return true;
	}
	
	public void Reset()
	{
		for(Word w : left)
		{
			w.SetQuantity(1);
		}
		
		for(Word w : right)
		{
			w.SetQuantity(1);
		}
	}
}
