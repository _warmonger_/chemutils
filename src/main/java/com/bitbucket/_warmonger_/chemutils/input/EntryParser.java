package com.bitbucket._warmonger_.chemutils.input;

import java.util.ArrayList;
import java.util.List;

public class EntryParser
{
	public static class IntBuffer
	{
		int obj;

		public IntBuffer(int obj)
		{
			this.obj = obj;
		}

		public IntBuffer()
		{
		}

		public int Get()
		{
			return obj;
		}

		public void Set(int obj)
		{
			this.obj = obj;
		}
	}

	public static class Buffer<T>
	{
		T obj;

		public Buffer(T obj)
		{
			this.obj = obj;
		}

		public Buffer()
		{
		}

		public T Get()
		{
			return obj;
		}

		public void Set(T obj)
		{
			this.obj = obj;
		}
	}

	public static Equation Parse(String s)
	{
		String[] ar = s.split("\\-\\>");
		
		List<Word> left, right;
		left = ParseLine(ar[0]);
		right = ParseLine(ar[1]);
		
		return new Equation(left, right);
	}
	
	public static List<Word> ParseLine(String s)
	{
		s = s.replaceAll("\\s+", "");
		
		int currIndex = 0;

		List<Word> res = new ArrayList<Word>();

		for(;currIndex < s.length();)
		{
			for(char c = s.charAt(currIndex);currIndex < s.length()		&& c == '+';currIndex++,c = s.charAt(currIndex))
				;

			Buffer<Word> buff = new Buffer<Word>();
			currIndex = ParseWord(s, currIndex, buff);

			res.add(buff.Get());
		}

		return res;
	}

	public static int ParseQuantity(String s, int currIndex, IEntry entry)
	{
		if (currIndex < s.length() && s.charAt(currIndex) == '[')
		{
			int startIndex = ++currIndex;

			for(;s.charAt(currIndex) != ']' && currIndex < s.length();currIndex++)
				;

			if (startIndex != currIndex)
			{
				entry.SetQuantity(Integer.parseInt(s.substring(startIndex, currIndex)));
			}
			else
			{
				entry.SetQuantity(1);
			}

			currIndex++;
		}

		return currIndex;
	}

	public static int ParseCharge(String s, int currIndex, IEntry entry)
	{
		if (currIndex < s.length() && s.charAt(currIndex) == '{')
		{
			currIndex++;
			boolean sign = true;

			if (!Character.isDigit(s.charAt(currIndex)))
			{
				sign = s.charAt(currIndex) != '-';

				currIndex++;
			}

			int startIndex = currIndex;
			for(;s.charAt(currIndex) != '}' && currIndex < s.length();currIndex++)
				;

			int charge = startIndex == currIndex ? 0 : Integer.parseInt(s.substring(startIndex, currIndex));

			entry.SetCharge((sign ? 1 : -1) * charge);

			currIndex++;
		}

		return currIndex;
	}

	public static int ParseWordEntry(String s, int currIndex, Buffer<IEntry> res)
	{
		if (s.charAt(currIndex) == '(')
		{
			Buffer<CompositeEntry> buff = new Buffer<CompositeEntry>();
			currIndex = ParseCompositeWord(s, currIndex, buff);

			res.Set(buff.Get());
		}
		else
		{
			Buffer<WordEntry> buff = new Buffer<WordEntry>();
			currIndex = ParseSimpleWord(s, currIndex, buff);

			res.Set(buff.Get());
		}

		return currIndex;
	}

	public static int ParseSimpleWord(String s, int currIndex, Buffer<WordEntry> res)
	{
		int firstIndex = currIndex;
		
		//System.out.println(s.substring(currIndex, s.length()));
		//System.out.format("%c: %b\n", s.charAt(currIndex), Character.isUpperCase(s.charAt(currIndex)));
		
		if (!Character.isUpperCase(s.charAt(currIndex)))
		{
			throw new IllegalArgumentException("Corrupted string! Missing uppercase letter of element.");
		}
		
		currIndex++;

		WordEntry entry = new WordEntry();

		for(;currIndex < s.length() && Character.isLetter(s.charAt(currIndex)) && Character.isLowerCase(s.charAt(currIndex));currIndex++);

		String element = s.substring(firstIndex, currIndex);
		entry.element = element;
		

		res.Set(entry);
		currIndex = ParseQuantity(s, currIndex, res.Get());
		currIndex = ParseCharge(s, currIndex, res.Get());

		return currIndex;
	}

	public static int ParseCompositeWord(String s, int currIndex, Buffer<CompositeEntry> res)
	{		
		CompositeEntry entry = new CompositeEntry();
		
		if (s.charAt(currIndex) != '(')
		{
			throw new IllegalArgumentException("Corrupted string! Missing '('.");
		}
		currIndex++;

		for(;currIndex < s.length();)
		{
			if (s.charAt(currIndex) == ')')
			{
				currIndex++;

				break;
			}

			Buffer<IEntry> buff = new Buffer<IEntry>();
			currIndex = ParseWordEntry(s, currIndex, buff);

			entry.AddEntry(buff.Get());
		}

		res.Set(entry);
		currIndex = ParseQuantity(s, currIndex, res.Get());
		currIndex = ParseCharge(s, currIndex, res.Get());
		
		return currIndex;
	}

	public static int ParseWord(String s, int currIndex, Buffer<Word> res)
	{
		int firstIndex = currIndex;
		for(;currIndex < s.length() && Character.isDigit(s.charAt(currIndex)) && s.charAt(currIndex) != '+';currIndex++)
			;

		String quantity = currIndex == firstIndex ? "1" : s.substring(firstIndex, currIndex);
		
		Word word = new Word(Integer.parseInt(quantity), 0);
		for(;currIndex < s.length() && s.charAt(currIndex) != '+';)
		{
			Buffer<IEntry> buff = new Buffer<IEntry>();
			currIndex = ParseWordEntry(s, currIndex, buff);
			
			word.AddEntry(buff.Get());
		}
		
		

		res.Set(word);
		currIndex = ParseCharge(s, currIndex, res.Get());
		
		return currIndex;
	}
}