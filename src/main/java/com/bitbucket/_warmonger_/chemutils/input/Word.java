package com.bitbucket._warmonger_.chemutils.input;

import java.util.ArrayList;
import java.util.List;

public class Word extends CompositeEntry
{
	public Word(int quantity, int charge)
	{
		super(quantity, charge);
	}
	
	public Word()
	{
	}
	
	@Override
	public String toString()
	{
		StringBuilder res = new StringBuilder();
		
		if(1 < quantity) { res.append(quantity); }		
		for(IEntry en : entries)
		{
			res.append(en.toString());
		}
		
		if(charge != 0)
		{
			res.append('{');
			res.append(charge);
			res.append('}');
		}
		
		return res.toString();
	}
	
	public List<IEntry> Unfold()
	{
		List<IEntry> res = new ArrayList<IEntry>();
		
		Unfold(res);
		
		return res;
	}
}
