package com.bitbucket._warmonger_.chemutils.input;

public abstract class Entry implements IEntry
{
	int    quantity = 1;
	int    charge   = 0;

	public Entry(int quantity, int charge)
	{
		this.quantity = quantity;
		this.charge = charge;
	}

	public Entry()
	{
	}

	public int GetQuantity()
	{
		return quantity;
	}

	public void SetQuantity(int quantity)
	{
		this.quantity = quantity;
	}

	public void UpdateQuantity(int change)
	{
		this.quantity += change;
	}

	public int GetCharge()
	{
		return charge;
	}

	public void SetCharge(int charge)
	{
		this.charge = charge;
	}

	public void UpdateCharge(int change)
	{
		this.charge += change;
	}
}
