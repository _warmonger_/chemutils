package com.bitbucket._warmonger_.chemutils.input;

import java.util.ArrayList;
import java.util.List;

public class CompositeEntry extends Entry
{
	List<IEntry> entries = new ArrayList<>();
	
	public CompositeEntry(int quantity, int charge, List<IEntry> entries)
	{
		super(quantity, charge);
		
		this.entries = entries;
	}
	
	public CompositeEntry(int quantity, int charge)
	{
		super(quantity, charge);
	}
	
	public CompositeEntry()
	{
	}
	
	@Override
	public String toString()
	{
		StringBuilder res = new StringBuilder();

		res.append('(');		
		for(IEntry en : GetEntries())
		{
			res.append(en.toString());
		}		
		res.append(')');
		
		if (1 < quantity)
		{
			res.append('[');
			res.append(quantity);
			res.append(']');
		}
		if(charge != 0)
		{
			res.append('{');
			res.append(charge);
			res.append('}');
		}

		return res.toString();
	}
	
	public void Unfold(List<IEntry> list)
	{
		for(IEntry en : entries)
		{
			en.Unfold(list);
		}
	}
	
	@Override
	public boolean DoesMatch(IEntry en1)
	{
		if(!(en1 instanceof CompositeEntry))
		{
			return false;
		}
		
		//todo
		
		return true;
	}
	
	public List<IEntry> GetEntries()
	{
		return entries;
	}

	public boolean AddEntry(IEntry entry)
	{
		for(IEntry en:entries)
		{
			if (en.DoesMatch(entry))
			{
				en.UpdateQuantity(entry.GetQuantity());

				return false;
			}
		}

		entries.add(entry);

		return true;
	}
}
