package com.bitbucket._warmonger_.chemutils.input;

import java.util.List;

public class WordEntry extends Entry
{
	String element = "";

	public WordEntry(String element, int quantity, int charge)
	{
		super(quantity, charge);

		this.element = element;
	}

	public WordEntry()
	{
	}

	@Override
	public String toString()
	{
		StringBuilder res = new StringBuilder();

		res.append(element);
		if (1 < quantity)
		{
			res.append('[');
			res.append(quantity);
			res.append(']');
		}
		if (charge != 0)
		{
			res.append('{');
			if (0 < charge)
			{
				res.append('+');
			}
			res.append(charge);
			res.append('}');
		}

		return res.toString();
	}

	public String GetElement()
	{
		return element;
	}

	public void Unfold(List<IEntry> list)
	{
		list.add(this);
	}

	@Override
	public boolean DoesMatch(IEntry en1)
	{
		return (en1 instanceof WordEntry) && ((WordEntry) en1).GetElement().equals(GetElement());
	}
}
