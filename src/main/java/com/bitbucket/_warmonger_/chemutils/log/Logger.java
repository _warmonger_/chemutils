package com.bitbucket._warmonger_.chemutils.log;

import java.io.PrintStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Logger implements ILogger
{
	private static final DateTimeFormatter dtFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
	
	PrintStream out;
	PrintStream err;

	public Logger(PrintStream out, PrintStream err)
	{
		this.out = out;
		this.err = err;
	}
	
	public Logger()
	{
		this(System.out, System.err);
	}

	public boolean Log(LogLevel level, String msg)
	{
		return Log(level, null, msg);
	}
	
	public boolean Log(LogLevel level, String title, String msg)
	{
		PrintStream str;

		switch (level.GetPreferredStreamID())
		{
			case LogLevel.STD_OUT:
				str = out;
				break;
			case LogLevel.STD_ERR:
				str = err;
				break;
			default:
				return false;
		}
		
		String currDateTime = LocalDateTime.now().format(dtFormatter);
		
		if(title == null)
		{
			str.format("[%s] [%s] %s\n", currDateTime, level.GetName(), msg);
		}
		else
		{
			str.format("[%s] [%s] %s - %s\n", currDateTime, level.GetName(), title, msg);
		}
		
		return true;
	}
}
