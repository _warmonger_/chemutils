package com.bitbucket._warmonger_.chemutils.log;

import java.io.PrintWriter;
import java.io.StringWriter;

public interface ILogger
{	
	public boolean Log(LogLevel level, String msg);	
	public boolean Log(LogLevel level, String title, String msg);
	
	static String Extract(Throwable t)
	{
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		t.printStackTrace(pw);
		
		return sw.toString();
	}
	
	default boolean Log(Throwable t)
	{
		return Log(LogLevel.ERROR, "Exception", Extract(t));
	}	
	
	default boolean Info(String msg)
	{
		return Log(LogLevel.INFO, msg);
	}
	default boolean Info(String title, String msg)
	{
		return Log(LogLevel.INFO, title, msg);
	}
	default boolean Info(String title, String msg, Object... params)
	{
		return Log(LogLevel.INFO, title, String.format(msg, params));
	}
	
	
	default boolean Warn(String msg)
	{
		return Log(LogLevel.WARN, msg);
	}	
	default boolean Warn(String title, String msg)
	{
		return Log(LogLevel.WARN, title, msg);
	}
	default boolean Warn(String title, String msg, Object... params)
	{
		return Log(LogLevel.WARN, title, String.format(msg, params));
	}
	
	default boolean Error(String msg)
	{
		return Log(LogLevel.ERROR, msg);
	}	
	default boolean Error(String title, String msg)
	{
		return Log(LogLevel.ERROR, title, msg);
	}
	default boolean Error(String title, String msg, Object... params)
	{
		return Log(LogLevel.ERROR, title, String.format(msg, params));
	}
}
