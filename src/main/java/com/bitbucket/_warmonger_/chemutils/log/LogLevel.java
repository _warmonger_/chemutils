package com.bitbucket._warmonger_.chemutils.log;

public enum LogLevel
{	
	INFO(
			0, "INFO", 0, 1
	), WARN(
			1, "WARN", 1, 1
	), ERROR(
			2, "ERROR", 2, 2
	), UNKNOWN(
			-1, "UNKNOWN", Integer.MAX_VALUE, 2
	);

	protected static final int STD_OUT = 1;
	protected static final int STD_ERR = 2;

	int                        ID;
	String                     name;
	int                        priority;
	int                        preferredStreamID;

	private LogLevel(int iD, String name, int priority, int preferredStreamID)
	{
		ID = iD;
		this.name = name;
		this.priority = priority;
		this.preferredStreamID = preferredStreamID;
	}

	public int GetID()
	{
		return ID;
	}

	public String GetName()
	{
		return name;
	}

	public int GetPriority()
	{
		return priority;
	}
	
	public int GetPreferredStreamID()
	{
		return preferredStreamID;
	}
}