package com.bitbucket._warmonger_.chemutils.math;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.linear.DecompositionSolver;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;
import org.apache.commons.math3.linear.SingularValueDecomposition;
import org.apache.commons.math3.util.ArithmeticUtils;
import org.apache.commons.math3.util.Precision;

import com.bitbucket._warmonger_.chemutils.input.Word;

public class Calculator
{
	public static int GetMult(int scale)
	{
		int res = 1;
		
		for(int i = 0; i < scale; i++)
		{
			res *= 10;
		}
		
		return res;
	}
	
	public static int[] Reduce(double[] ar, int scale)
	{
		long[] convAr = new long[ar.length];
		int mult = GetMult(scale);
		long gcd = 0;
		
		for(int i = 0; i < ar.length; i++)
		{
			convAr[i] = (long) (Precision.round(ar[i], scale) * mult);
			
			gcd = gcd == 0 ? convAr[i] : ArithmeticUtils.gcd(gcd, convAr[i]);
		}
		
		int[] res = new int[ar.length];
		for(int i = 0; i < ar.length; i++)
		{
			res[i] = (int) (convAr[i] / gcd);
		}
		
		return res;
	}
	
	public static RealVector GetResultVector(int len)
	{
		double[] ar = new double[len];

		for(int i = 1;i < ar.length;i++)
		{
			ar[i] = 0;
		}		
		ar[0] = 1;

		return MatrixUtils.createRealVector(ar);
	}

	public static RealMatrix GetCoreMatrix(List<Word> leftSide, List<Word> rightSide, List<String> elements)
	{
		Converter.GetElements(elements, leftSide);
		Converter.GetElements(elements, rightSide);
		
		RealMatrix left = MatrixUtils.createRealMatrix(elements.size() + 2, leftSide.size() + rightSide.size());
		RealMatrix right = MatrixUtils.createRealMatrix(elements.size() + 2, leftSide.size() + rightSide.size());
		left.setSubMatrix(Converter.Convert(elements, leftSide), 2, 0);
		right.setSubMatrix(Converter.Convert(elements, rightSide), 2, leftSide.size());

		RealMatrix res = left.subtract(right);
		res.setEntry(0, 0, 1);
		
		int i = 0;
		for(Word w : leftSide)
		{
			res.setEntry(1, i++, w.GetCharge());
		}
		for(Word w : rightSide)
		{
			res.setEntry(1, i++, w.GetCharge());
		}
		
		return res;
	}

	public static int FindGCD(int a, int b)
	{
		int swap;

		while (b != 0)
		{
			swap = a;
			a = b;
			b = swap % b;
		}

		return a;
	}

	public static RealVector Calc(RealMatrix core, RealVector coeffs)
	{
		DecompositionSolver solver = new SingularValueDecomposition(core).getSolver();

		return solver.solve(coeffs);
	}

	public static int[] Calc(List<Word> leftSide, List<Word> rightSide)
	{
		List<String> elements = new ArrayList<String>();
		
		RealMatrix core = GetCoreMatrix(leftSide, rightSide, elements);
		RealVector coeffs = GetResultVector(core.getRowDimension());
		
		DecompositionSolver solver = new SingularValueDecomposition(core).getSolver();
		
		RealVector res0 = solver.solve(coeffs);
		
		return Reduce(res0.toArray(), 8);
	}
}
