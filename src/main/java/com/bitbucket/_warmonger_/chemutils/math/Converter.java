package com.bitbucket._warmonger_.chemutils.math;

import java.util.List;

import org.apache.commons.math3.fraction.Fraction;

import com.bitbucket._warmonger_.chemutils.input.CompositeEntry;
import com.bitbucket._warmonger_.chemutils.input.IEntry;
import com.bitbucket._warmonger_.chemutils.input.Word;
import com.bitbucket._warmonger_.chemutils.input.WordEntry;

public class Converter
{
	public static void GetElements(CompositeEntry c, List<String> res)
	{
		for(IEntry en:c.GetEntries())
		{
			if (en instanceof WordEntry)
			{
				String s = ((WordEntry) en).GetElement();

				if (!res.contains(s))
				{
					res.add(s);
				}
			}
			else if (en instanceof CompositeEntry)
			{
				GetElements((CompositeEntry) en, res);
			}
		}
	}

	public static void GetElements(List<String> res, List<Word> list)
	{
		for(Word w:list)
		{
			GetElements(w, res);
		}
	}

	public static int CountElement(String s, CompositeEntry c)
	{
		int res = 0;

		for(IEntry en:c.GetEntries())
		{
			if (en instanceof WordEntry)
			{
				if (((WordEntry) en).GetElement().equals(s))
				{
					res += en.GetQuantity();
				}
			}
			else if (en instanceof CompositeEntry)
			{
				res += CountElement(s, (CompositeEntry) en);
			}
		}

		return res * c.GetQuantity();
	}

	public static double[][] Convert(List<String> el, List<Word> list)
	{
		double[][] ar = new double[el.size()][];
		ar[0] = new double[list.size()];

		for(int i = 0;i < el.size();i++)
		{
			ar[i] = new double[list.size()];

			for(int j = 0;j < list.size();j++)
			{
				ar[i][j] = CountElement(el.get(i), list.get(j));
			}
		}

		for(int i = el.size();i < el.size();i++)
		{
			ar[i] = new double[list.size()];

			ar[i][i - el.size()] = 1;
		}

		return ar;
	}
	
	public static Fraction[][] Convert2(List<String> el, List<Word> list)
	{
		Fraction[][] ar = new Fraction[el.size()][];
		ar[0] = new Fraction[list.size()];

		for(int i = 0;i < el.size();i++)
		{
			ar[i] = new Fraction[list.size()];

			for(int j = 0;j < list.size();j++)
			{
				ar[i][j] = new Fraction(CountElement(el.get(i), list.get(j)));
			}
		}

		for(int i = el.size();i < el.size();i++)
		{
			ar[i] = new Fraction[list.size()];

			ar[i][i - el.size()] = Fraction.ONE;
		}

		return ar;
	}
}
