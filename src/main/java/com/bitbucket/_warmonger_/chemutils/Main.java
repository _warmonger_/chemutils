package com.bitbucket._warmonger_.chemutils;

import com.bitbucket._warmonger_.chemutils.app.ChemUtilsApp;
import com.bitbucket._warmonger_.chemutils.log.ILogger;
import com.bitbucket._warmonger_.chemutils.log.Logger;

public class Main
{
	public static final String TITLE;
	public static final String VERSION;
	public static final String URL;
	
	static
	{
	    Package mainPackage = Main.class.getPackage();
	    
	    TITLE = mainPackage.getName();
	    VERSION = mainPackage.getImplementationVersion();
	    URL = mainPackage.getImplementationTitle();
	}
	
	public static void main(String[] args)
	{
		ChemUtilsApp app;
		ILogger logger = new Logger();
		
		try
		{
			app = new ChemUtilsApp(logger);
			
			app.Open();
		}
		catch(Throwable t)
		{
			logger.Log(t);
			
			return;
		}
		
		logger.Info("Application has been run successfully");
	}
}
