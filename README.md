#### ChemUtils

Chemistry Utilities Tool is a GUI application which consists of utilities for chemistry tasks and research.



## Stoichiometry

# Balancing reaction

Enter left and right sides of reaction into text fields. You can find an example below:

Cu + HNO₃
Cu(NO₃)₂ + NO₂ + H₂O

Subscripts and superscripts are emtered automatically.

Email: michal.s.jagodzinski@gmail.com